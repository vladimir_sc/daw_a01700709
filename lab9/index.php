<?php
  function promedio($num) {
    $p = array_sum($num)/count($num);

    return $p;
  }

  function mediana($num) {
    sort($num);

    $n = count($num)/2;

    return $num[$n];
  }

  function lista($num) {
    $r = $num;

    $lista = "<h5> Lista </h5> <ul>";

    rsort($r);
    for($i = count($num); $i > 0; $i--) {
      $lista .= "<li>".array_pop($r)."</li>";
    }

    sort($num);
    for($i = count($num); $i > 0; $i--) {
      $lista .= "<li>".array_pop($num)."</li>";
    }

    $lista .="</ul>";

    return $lista;
  }

  function potencias($n) {
    $tabla = "<h5>Tabla de potencias</h5>";
    $tabla .= "<table>";
    $tabla .= "<thead>";
    $tabla .= "<tr>";
    $tabla .= "<th>Numero</th><th>Cuadrado</th><th>Cubo</th>";
    $tabla .= "</tr>";
    $tabla .= "</thead><tbody>";

    for ($i = 1; $i <= $n; $i++) {
      $tabla .= "<tr>";
      $tabla .= "<td>$i</td><td>".$i*$i."</td><td>".$i*$i*$i."</td>";
      $tabla .= "</tr>";
    }

    $tabla .= "</tbody></table>";

    return $tabla;
  }

  include("_header.html");

  echo "<main>";

  $a = array(1,7,22,6,9,10);

  //Problema 1
  echo "<h5> Promedio </h5>";
  $p = prom($a);
  echo $p."<br>";

  //Problema 2
  echo "<h5> Mediana </h5>";
  $med = mediana($a);
  echo $med."<br>";

  //Problema 3
  $lista = lis($a);
  echo $lista."<br>";

  //Problema 4
  $tabla = potencias(10);
  echo $tabla."<br>";

  echo "</main>";

  include "_footer.html";
?>
