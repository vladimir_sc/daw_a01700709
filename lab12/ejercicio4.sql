INSERT INTO entregan values (1000, 'AAAA800101', 5000, GETDATE(), 0); 

SELECT *
	FROM Entregan;

/* �Qu� uso se le  esta dando a GETDATE()?
	Esta tomando la fecha que esta registrada en el servidor

	�Tiene sentido el valor del campo cantidad?
	No, ya que no estamos entregando nada.
*/

Delete from Entregan where Cantidad = 0;

ALTER TABLE entregan add constraint cantidad check (cantidad > 0); 

/* �C�mo responde el sistema?
	The INSERT statement conflicted with the CHECK constraint "cantidad". The conflict occurred in database "a1700709", table "a1700709.Entregan", column 'Cantidad'.

	�Qu� significa el mensaje?
	No permite insertarlo ya que pide checar el constraint "Cantidad" debido a que ocurre un conflicto
*/

/* Integridad referencial
   Tiene una importancia en las llaves foraneas, esto quiere decir que debe existir un campo valido en la tabla a la que se haga referencia
   esto para garantizar la referencia de datos entre las tuplas de las tablas.
*/