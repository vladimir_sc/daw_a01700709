-- Inserta un nuevo registro a la tabla Materiales
INSERT INTO Materiales values(1000, 'xxx', 1000)

/* �Cu�l es?
	Falta de integridad referencial
	
	�A qu� se debe?
	A qu� existen dos registros con la misma clave
*/

-- Elimina el registro que tiene clave 1000 y un Costo de 1000
Delete from Materiales where Clave = 1000 and Costo = 1000 

-- Hace que la columna "Clave" de materiales se convierta en una llave primaria
ALTER TABLE Materiales add constraint llaveMateriales PRIMARY KEY (Clave) 

-- Intentamos insertar la tupla 
INSERT INTO Materiales values(1000, 'xxx', 1000)

/* �Qu� ocurrio?
	No nos permite insertar la tupla ya que existe una con dicha clave
	Se debe a que la columna "Clave" fue definida como Llave primaria
*/

sp_helpconstraint materiales
/* �Qu� informaci�n muestra esta consulta?
	Nos muestra las caracteristicas de los Constraints de la tabla materiales, en este caso nos muestra que la Columna Clave es una llave primaria
*/

-- �Qu� sentencias utilizaste para definir las llaves primarias? 
ALTER TABLE Proveedores add constraint llaveProveedores PRIMARY KEY (RFC) 
ALTER TABLE Proyectos add constraint llaveProyectos PRIMARY KEY (Numero) 

-- �Qu� sentencias utilizaste para definir este constrait? 
ALTER TABLE Entregan add constraint llaveEntregan PRIMARY KEY (Clave, RFC, Numero, Fecha) 