INSERT INTO entregan Values (0, 'xxx', 0, '1-jan-02', 0); 

SELECT * from entregan;

/* �Qu� particularidad observas en los valores para Clave, RFC y Numero?
	Que en todas las demas tuplas existen un valor �nico, en el caso de la que acabamos de insertar no son muy eficientes para garantizar su unicidad

	�C�mo reacciona el sistema a la inserci�n de este registro?
	El sistema lo ingresa de manera eficiente
*/

Delete from Entregan where Clave = 0 

ALTER TABLE entregan add constraint cfentreganclave 
  foreign key (clave) references materiales(clave);

/* �Qu� significa el mensaje que emite el sistema?
	Nos dice que hay un problema al intentar insertar el registro ya que hay un error con la tabla Materiales

	�Qu� significa la sentencia anterior?
	Que declaramos la columna "Clave" de Entregan como llave foranea de Materiales	
*/

ALTER TABLE entregan add constraint cfentreganRFC 
  foreign key (RFC) references proveedores(RFC);

ALTER TABLE entregan add constraint cfentregannumero 
  foreign key (numero) references proyectos(numero);

sp_helpconstraint materiales
sp_helpconstraint proveedores
sp_helpconstraint proyectos
sp_helpconstraint entregan

/* �Qu� significan las columnas de esas consultas?
	Nos muestra las Columnas que estan referenciadas en otras tablas como llaves foraneas, en el caso de Entregan nos muestra las llaves foraneas que se han creado
*/




