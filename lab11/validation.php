<?php
    if(isset($_POST["Nombre"]) && isset($_POST["Edad"]) && isset($_POST["Carrera"]) && isset($_POST["Campus"])) {
        if($_POST["Edad"] > 17 && $_POST["Edad"] < 26 ) {
            $nombre = $_POST["Nombre"];
            $edad = $_POST["Edad"];
            $carrera = $_POST["Carrera"];
            $campus = $_POST["Campus"];

            include("_header.html");
            include("_aceptado.html");
            include("_footer.html");
        }
        else {
            header("HTTP/1.0 404 Not Found");
        }
    }
    else {
        header("location:index.php");
    }
?>
